<?php

/**
 * Configuration form callback.
 */
function csp_nonce_configuration_form($form, &$form_state) {
  $form['csp_nonce_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable CSP header.'),
    '#default_value' => variable_get('csp_nonce_enabled', TRUE),
  );
  $form['csp_nonce_add_to_scripts'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add nonce to all script tags rendered by Drupal.'),
    '#default_value' => variable_get('csp_nonce_add_to_scripts', TRUE),
  );
  $form['csp_nonce_policy'] = array(
    '#title' => t('Policy'),
    '#type' => 'textarea',
    '#description' => t('Available token: "@nonce".'),
    '#default_value' => variable_get('csp_nonce_policy', CSP_NONCE_DEFAULT_POLICY),
  );
  return system_settings_form($form);
}
